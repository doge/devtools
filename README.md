devtools
========

bootstrap-pycharm.sh
--------------------

Requires:

* Oracle JDK

Installs PyCharm to /opt/ and adds a symlink in /usr/local/sbin
