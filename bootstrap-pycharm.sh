#!/bin/bash

# Get pycharm
wget -O /tmp/pycharm-community-3.0.2.tar.gz http://download.jetbrains.com/python/pycharm-community-3.0.2.tar.gz
cd /opt/
tar -xzvf /tmp/pycharm-community-3.0.2.tar.gz
ln -s pycharm-community-3.0.2 pycharm
cd /usr/local/sbin
ln -s /opt/pycharm/bin/pycharm.sh pycharm
